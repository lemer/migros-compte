# Comptes
Une appli CLI pour tenir des comptes entre un petit groupe de gens.

## IMPORTANT:
NE PAS MODIFIER LE FICHIER `infos` A LA MAIN!

## User:
Clone the repo, run:
* Unix/Mac: `gradlew assemble`
* Windows: `gradlew.bat assemble`
The copy the `infos` file and the jar file in `build/libs/` to where you want to keep them. You can launch the application either through the command line using `java -jar <jarname>.jar` (with the same CWD as the `infos` file) or simply by double-clicking on the jar file.

## Developer:
Even though you can also work with the gradlew (gradle-wrapper) script, you'll probably want to install gradle. To build the project with the tests and static code analysis, run
`gradle build` and then `gradle javadoc`. The above commands ofc also work for you.

### Gradle 101:
* build.gradle -- file configuring different taks. ≃ Makefile
* gradle directory -- contains things proper to gradle. Don't touch.
* gradlew -- script for poeple who don't have gradle installed, DL's a smaller version that enables them to do a reproducible build.
* gradlew.bat -- same for idiots who run Windaube.
* build dir -- output directory
* src -- contains Main and Test, each with a java/ and resources/ directories. Source sets.
Also, read the [docs](https://docs.gradle.org)!!
