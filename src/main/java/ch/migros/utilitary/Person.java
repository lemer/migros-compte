package ch.migros.utilitary;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public final class Person{
    private final String name;
    private final Map<Person, Double> owes;
    private static MsgProvider msgs = new MsgProvider();

    public Person(String n){
        name = n;
        owes = new HashMap<>();
    }

    public void removePerson(Person p) {
        owes.remove(p);
    }



    public Person(Person p){
        this(p.name);
    }


    public boolean paid(Person p, double amount) {
        if(p.equals(this)){
            return true;
        }
        double debt;
        if(owes.containsKey(p)){
            debt = owes.get(p);
        }else{
            owes.put(p,0.);
            debt = 0;
        }
        debt -= amount;
        if(debt < 0){
            p.owes(this, -debt);
            debt = 0;

            owes.remove(p);
            return true;
        }
        if(debt == 0){
            owes.remove(p);
            return true;
        }
        owes.put(p, debt);
        return true;
    }

    public Map<Person, Double> getOwes(){
        return owes;
    }

    public boolean owes(Person payer, double owed) {
        if(owes.containsKey(payer)){
            owes.compute(payer, (k,v) -> v+owed);
        }else{
            owes.put(payer, owed);
        }
        return true;
    }


    public String name(){
        return name;
    }

    @Override
    public String toString(){
        return name;
    }

    @Override
    public boolean equals(Object o){
        return o instanceof Person && o.hashCode() == hashCode();
    }
    @Override
    public int hashCode(){
        return Objects.hash(name, owes.values());
    }
}
