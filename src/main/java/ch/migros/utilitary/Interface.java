
package ch.migros.utilitary;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import ch.migros.commands.*;

public final class Interface{
    private final Map<String, Command> COMMANDS = new HashMap<>();
    private Logger logger;
    private boolean running;
    private FileManager fM;
    private boolean debugMode;
    private final String fileIn = "infos";
    private String fileOut = fileIn;
    private MsgProvider msgs = new MsgProvider();
    private Scanner scan = new Scanner(System.in);

    public Interface(){
        fM = new FileManager(new File(fileIn));
        this.logger = fM.open();

        Set<Command> cmds = new HashSet<>();
        cmds.addAll(Arrays.asList(
                new DebugCmd(this, logger),
                new HelpCmd(cmds),
                new AddCmd(logger),
                new QuitCmd(this),
                new DebtsCmd(logger),
                new LogCmd(logger),
                new LogsCmd(logger),
                new NamesCmd(logger),
                new ReimburseCmd(logger),
                new RemoveCmd(logger),
                new BackupCmd(this),
                new WhosNext(logger),
                new Alias("exit", new QuitCmd(this), null)
                ));
        cmds.forEach(c -> COMMANDS.put(c.toString(), c));

        running = true;
    }

    public void close(){
        running = false;
    }

    public void run(){
        running = true;
        List<String> instr;
        String cmd;
        List<String> options;
        do{
            System.out.print(msgs.prompt());
            String str = scan.nextLine();
            instr = getInstr(str);

            if (instr.size() == 0) {
                System.out.println(msgs.emptyCommand());
                continue;
            }

            cmd = instr.get(0);

            if(instr.size() > 1){
                options = instr.subList(1, instr.size());
            }else{
                options = new ArrayList<>();
            }

            Command action = COMMANDS.get(cmd);

            if(action != null){
                if(!action.run(options)){
                    try{
                        Thread.sleep(10);
                        System.err.println(msgs.noCommandExecuted());
                        Thread.sleep(10);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }else{
                System.out.println(msgs.unknownCommand(cmd));
                running = true;
            }
        }while(running);

        if(logger.hasChanged()){
            fM.save(fileOut);
        }else{
            System.out.println(msgs.noChangesToSave());
        }
    }

    public void debugMode(boolean set){
        debugMode = set;
        if(debugMode){
            fileOut = ".debug";
        }else{
            fileOut = fileIn;
        }
    }
    public boolean debugMode(){
        debugMode(!debugMode);
        return debugMode;
    }

    public void setFileOut(String s){
        fileOut = s;
    }
    public String getFileIn(){
        return fileIn;
    }
    public FileManager getFM(){
        return fM;
    }
    public Logger getLogger(){
        return logger;
    }

    private List<String> getInstr(String str){
        StringBuilder cmdB = new StringBuilder();
        List<String> instrs = new ArrayList<>();

        for(char a : str.toCharArray()){
            if(a == ' '){
                if(cmdB.length() > 0){
                    instrs.add(cmdB.toString());
                }
                cmdB.setLength(0);
            }else{
                cmdB.append(a);
            }
        }
        if(cmdB.length() != 0){
            instrs.add(cmdB.toString());
        }
        return instrs;
    }
}

