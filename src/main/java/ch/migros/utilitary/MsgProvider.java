package ch.migros.utilitary;

import ch.migros.commands.Command;

public final class MsgProvider{
    private final Command cmd;

    public MsgProvider(Command cmd){
        this.cmd = cmd;
    }
    public MsgProvider(){
        this.cmd = null;
    }

 // Common messages.
    public String prompt(){ return Color.GREEN.applyTo("Hello ") + "$ "; }
    public String noChangesToSave(){ return "No changes have been made to the file."; }
    public String debugModeEnabled(){ return "Debug mode enabled. All changes will now be saved in a debug file.";}
    public String debugModeDisabled(){ return "Debug mode disabled. All changes will now be saved in the main file.";}
    public String saveWasSuccessful(String fileName){ return "Wrote successfully all changes in "+fileName;}

 // Not found messages
    public String personNotFound(String p){ return Color.RED.applyTo("Unknown person : " + p); }
    public String noLogsToList(){ return Color.RED.applyTo("There is no logs yet!"); }
    public String noNamesToList(){ return "There is no one there!"; }
    public String unknownCommand(String name){ return "Unknown command : "+name+"\n"+emptyCommand(); }
    public String noSuchLog(int index){ return "No log found with index "+index; }
    public String unknownOption(String s){ return "Unknown option : "+s+"\nType "+cmd.title+" -h to get all available";}

    // Invalid arguments
    public String expectedOnOff(String butWas){ return "Unknown argument : "+butWas+". This command is expecting \"on\" or \"off\"."; }
    public String invalidOptionFormat(Option o){ return "Invalid option format : "+o.name() +"\n type "+cmd.title+" -h for help." ; }
    public String invalidNumberOfArgs(){ return "Invalid number of arguments!\n"+cmd.format(); }
    public String invalidNumberFormat(){ return "Invalid number format!"; }
    public String emptyCommand(){ return "Type 'help' to get a list of all available commands."; }
    public String optionsAreNotFirst(){ return "Wrong command format! All options have to be right after the command name.";}

    // Fails to execute something
    public String backupOverMainFile(String name) { return "You are trying to make a backup over the main file : "+name; }
    public String isNotADouble(String s){ return s+" is not a double.\n"+cmd.format();}
    public String failToAddPerson(String n){ return "Impossible to add "+n+" to the list. Maybe "+n+" already exists."; }
    public String noDebts(Person p1){ return p1+" is already good."; }
    public String noCommandExecuted() { return Color.RED.applyTo("No command has been executed."); }

    // Bullshit
    public String youFool(){ return "You fool."; }
}
