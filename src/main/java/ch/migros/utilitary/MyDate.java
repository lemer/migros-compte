package ch.migros.utilitary;

import java.util.Calendar;
import java.util.TimeZone;

public class MyDate implements Comparable<MyDate>{
    public final int DAY;
    public final int MONTH;
    public final int YEAR;
    public static Calendar cal = Calendar.getInstance(TimeZone.getDefault());

    public MyDate(int y , int m, int d){
        this.DAY = normalize(d, 31);
        this.MONTH = normalize(m, 12);
        this.YEAR = normalize(y,-1);
    }

    public MyDate(){
        DAY = cal.get(Calendar.DAY_OF_MONTH);
        MONTH = cal.get(Calendar.MONTH)+1;
        YEAR = cal.get(Calendar.YEAR);
    }

    /**
     * Normalizes n to be strictly positive and <= to max. if max negative, n can be as big as desired.
     */
    private int normalize(int n, int max){
        if (max < 0){
            return n;
        }
        if ( max > 0 && n > max ){
            return max;
        } else if ( n < 1 ) {
            return 0;
        }
        return n;
    }

    @Override
    public String toString(){
        return String.format("%02d/%02d/%02d", DAY,MONTH,YEAR%100);
    }

    @Override
    public boolean equals(Object o){
        return o instanceof MyDate && compareTo((MyDate)o) == 0;
    }

    @Override
    public int compareTo(MyDate that){
        int yearComp = Integer.compare(YEAR, that.YEAR);
        if ( yearComp == 0) {

            int monthComp = Integer.compare(MONTH, that.MONTH);
            if ( monthComp == 0 ){

                return Integer.compare(DAY, that.DAY);
            } else {
                return monthComp;
            }
        }else{
            return yearComp;
        }


    }
}
