package ch.migros.utilitary;

import ch.migros.commands.Command;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Option{
    private final String name;
    private final String shortName;
    private final String help;
    private final Map<String, String> arguments;

    public Option(String name, String shortName, String help, Map<String, String> arguments){
        this.name = name;
        this.shortName = shortName;
        this.help = help;
        this.arguments = arguments;
    }

    public Option(String name, String shortName, String help){
        this(name, shortName, help, Collections.emptyMap());
    }

    public Option(String name, String help){
        this(name, name.charAt(0)+"", help, Collections.emptyMap());
    }

    public Option(String name, Command cmd){
        this(name, name.charAt(0)+"", Command.helpInfos.get(cmd.title).get(name), Collections.emptyMap());
    }

    public String name(){
        return name;
    }
    public String shortName(){
        return shortName;
    }

    public String help(){
        return help;
    }

    public int nbArgs(){
        return arguments.size();
    }

    @Override
    public boolean equals(Object o){
        boolean toReturn = o instanceof Option;
        if(toReturn){
            Option that = (Option)o;
            return that.name.equals(name) && that.arguments.equals(arguments);
        }
        return false;
    }
}
