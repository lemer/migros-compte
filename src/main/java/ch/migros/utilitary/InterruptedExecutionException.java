package ch.migros.utilitary;

public class InterruptedExecutionException extends Exception{
    /**
     * Default serial number.
     */
    private static final long serialVersionUID = 1L;

    public InterruptedExecutionException(){
        super("An unexpected and unknown problem has occured while execution!");
    }
    public InterruptedExecutionException(String message){
        super(message);
    }

    public InterruptedExecutionException withMessage(String s){
        return new InterruptedExecutionException(s);
    }
}
