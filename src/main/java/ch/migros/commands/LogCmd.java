package ch.migros.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Arrays;

import ch.migros.utilitary.Logger;
import ch.migros.utilitary.Option;
import ch.migros.utilitary.InterruptedExecutionException;

public class LogCmd extends Command{
    public static final String TITLE = "log";

    private Logger logger;
    private final Option OPT_n = option("message", "m", TITLE, Arrays.asList("message_content"));

    private static String help = "Logs a new transaction.";

    public LogCmd(Logger logger) {
        super("log", argsMoreThan(2));
        addArgument("<who>");
        addArgument("<amount>");
        addArgument("<toWhom>");
        addOption(OPT_n);
        this.logger = logger;
    }

    @Override
    public boolean tryToRun(Map<Option, List<String>> askedOptions, List<String> args) throws InterruptedExecutionException{
        String who = args.get(0);
        double amount = 0;

        try{
            amount = Double.parseDouble(args.get(1));
        } catch (NumberFormatException | NullPointerException exc) {
            throw new InterruptedExecutionException(msgs.invalidNumberFormat());
        }

        List<String> owers;
        if(args.size() == 2){
            owers = new ArrayList<>(logger.getNames());
        }else {
            owers = new ArrayList<>(args.subList(2, args.size()));
        }


        if(!logger.peopleContains(who)){
            throw new InterruptedExecutionException(msgs.personNotFound(who));
        }
        for(String n : owers){
            if(!logger.peopleContains(n)){
                throw new InterruptedExecutionException(msgs.personNotFound(n));
            }
        }

        // Output a confirmation demand.
        StringBuilder strB = new StringBuilder();
        strB.append(String.format("You're saying %s paid CHF%.2f in total for", who, amount));

        boolean himselfIn = false;

        // Iterate on people who owe money
        if (owers.size() == 1) {
            strB.append(" "+owers.get(0));
            if (owers.get(0).equals(who)) {
                himselfIn = true;
            }
        } else {
            int i=0;
            for (String p : owers) {
                if (!himselfIn && p.toLowerCase().equals(who.toLowerCase())) {
                    himselfIn = true;
                }
                ++i;
                if (i== owers.size()) {
                    strB.append(" and");
                }
                strB.append(" "+p);
            }
        }

        // Check if the guy who payed is in the owers.
        if (!himselfIn) {
            strB.append(", and did not pay anything for himself");
        }
        strB.append("?");

        String logName;

        if (askedOptions.containsKey(OPT_n)){
            logName = askedOptions.get(OPT_n).get(0);
        }else{
            logName = "Untitled";
        }

        /*
        //TODO deal with the -m option. Also add_command
        String logName = askMore("What do you want to call this log?");
        if (logName.equals("")){
            logName = "Untitled";
            }*/

        // Write the entry to the log.
        if (ask(strB.toString())) {
            return logger.log(logName, who, amount, owers);
        }
        return false;

    }

}
