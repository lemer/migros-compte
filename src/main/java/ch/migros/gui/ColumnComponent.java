package ch.migros.gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ch.migros.utilitary.Logger;

public class ColumnComponent{
    private Logger logger;
    private List<JComponent> components;
    private String title;

    public ColumnComponent(Logger logger, String title, List<JComponent> components){
        this.components = new ArrayList<>(components);
        this.logger = logger;
        this.title = title;
    }

    public static JComponent get(Logger logger, String title, List<JComponent> components){
        JPanel toReturn = new JPanel();
        BoxLayout layout = new BoxLayout(toReturn, BoxLayout.Y_AXIS);
        toReturn.setLayout(layout);

        toReturn.add(new JLabel(title));
        components.forEach( c -> toReturn.add(c));
        return toReturn;
    }
}
