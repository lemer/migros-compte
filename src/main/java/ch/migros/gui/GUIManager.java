package ch.migros.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import ch.migros.utilitary.InterruptedExecutionException;
import ch.migros.utilitary.Log;
import ch.migros.utilitary.Logger;
import ch.migros.utilitary.Person;

/**
   Manages all the GUI part of the application

   is capable of launching the window and putting all components in place.

   also stocks all common infomations important for all components such as font, colors, sizes  etc

   Components are :
   People
   Logs
   Both implement a component having a title and a vertical list of components.

   People list component :
   In a vertical list, there are the name of the payer (pluss a cross if component is hovered) and then all his debts.

   Debts component :
   horizontal list of 3 elements : amount, color, name of owed person.


   "+" button component :
   identical for both people and logs components, positioned in a layer fashion, taking a popup component.


   Logs list component :
   In a horizontal list, 3-4 elements : name, amount, names separeted by commas, delete cross if component is hovered


   popup component, abstract :
   only the white part of the popup. The gray and all is from the main component : when it is told to display a popup it creates a new layer on which is puts a gray background and in the center the pupop.
   already implements the presence of buttons (absolute positions) and margins, and leaves the certer empty to be filled by sub-classes, but has abstract methoed for the OK behaviour

   add name popup :
   name textfield.

   add log popup :
   two textfields
   selection component for selecting names.
   selection component for selecting all. both listen to each other.

   textField component, extends the text field JComponent :
   takes the gray name as argument,

   selection component, extends the box checking JComponent :
   changes its design and takes another component as parameter.



 */
public class GUIManager{
    private final Logger logger;
    private boolean opened;

    private static final int peopleWidth = 300;
    private static final int debtsLeftW = 150;
    private static final int debtsRightW = 150;
    private static final int stdRowH = 30;
    private static final int widnowheight = 600;
    private static final int addBtnSize = 40;
    private static final int peopleVertBar = 2;
    private static final int addBtnPadding = 16;

    private static final int logsWidth = 500;
    private static final int logs1W = 90;
    private static final int logs2W = 60;
    private static final int logs3W = 330; 

    public static final Color green = new Color(193, 237, 203);
    public static final Color red = new Color(240, 202, 184);
    public static final Color lightGray = new Color(230, 230, 230);
    public static final Color gray = new Color(127, 127, 127);
    public static final Font stdFont = new Font("Helvetica", Font.PLAIN, 15);
    public static final Font TitleFont = stdFont.deriveFont(23f);

    public Consumer<String> removeName;
    public Consumer<String> removeLog;

    public GUIManager(Logger logger){
        this.logger = logger;
        removeName = null;
    }

    public void openWindow() throws InterruptedExecutionException{
        if(opened){
            throw new InterruptedExecutionException("Window alerady opened.");
        }
        opened = true;
        SwingUtilities.invokeLater(() -> createUI());
    }

    private void createUI(){
        JFrame frame = new JFrame("Debts Manager");

        JComponent sections = new JPanel(new FlowLayout());

        JPanel people = new JPanel();
        people.setLayout(new BorderLayout());

        JPanel logs = wPanel();
        logs.setLayout(new BorderLayout());

        removeName = n -> {
            try {
                int response = JOptionPane.showConfirmDialog(null, "Are you sure?");
                if(response == JOptionPane.OK_OPTION){
                    logger.removePerson(n);
                } else {
                    JOptionPane.showMessageDialog(null, "No worries. Nothing has been done.");
                    return;
                }
            } catch (InterruptedExecutionException e1) {
                System.err.println(e1.getMessage());
            }
            SwingUtilities.invokeLater(() -> {updatePeople(people); updateLogs(logs);});
        };


        List<Log> logsList = new ArrayList<>(logger.getLogs());
        removeLog = s -> {
            int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this log?\n"+logsList.get(Integer.parseInt(s)));
            if(response == JOptionPane.OK_OPTION){
                logger.removeLog(logsList.get(Integer.parseInt(s)));
            } else {
                JOptionPane.showMessageDialog(null, "No worries. Nothing has been done.");
                return;
            }
            SwingUtilities.invokeLater(() -> {updatePeople(people); updateLogs(logs);});
        };

        updatePeople(people);
        updateLogs(logs);

        sections.add(people);
        sections.add(logs);

        frame.setContentPane(sections);
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        boolean decorated = true;
        if(!decorated){
            frame.setUndecorated(true);
        }
        frame.pack();
        frame.setPreferredSize(new Dimension(peopleWidth+logsWidth, widnowheight));
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);

        frame.setVisible(true);
    }

    private void updateLogs(JComponent logs){
        logs.removeAll();

        JComponent columnLogs = column(logger, "Logs", new Dimension(logsWidth, widnowheight- addBtnSize-addBtnPadding), logsList());

        logs.add(columnLogs, BorderLayout.CENTER);
        logs.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, gray));
        
        
        Consumer<String> addLogCmd = s -> JOptionPane.showOptionDialog(null, "You are adding a log", "New Log", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

        CrossButton addLogBtn = new CrossButton(addBtnSize, 1, "addLog", CrossButton.ADD, s -> JOptionPane.showMessageDialog(null, "Not yet implemented. Don't ever use a graphical interface."));
        addLogBtn.addMouseListener(new AddBtnMouseListener(addLogBtn));
        JPanel addBtnPane = wPanel();
        addBtnPane.setBorder(BorderFactory.createEmptyBorder(0, addBtnPadding, addBtnPadding, addBtnPadding));
        addBtnPane.setLayout(new BorderLayout());
        addBtnPane.add(addLogBtn, BorderLayout.EAST);

        logs.add(addBtnPane, BorderLayout.SOUTH);

        System.out.println(logger.getLogs().size());

        logs.repaint();
        logs.revalidate();
        logs.doLayout();
    }

    private JComponent logsList(){
        JPanel toReturn = boxPanel(BoxLayout.Y_AXIS);

        Set<Log> logs = new TreeSet<>(logger.getLogs());
        int index = 0;
        for(Log l : logs){
            JPanel element = boxPanel(BoxLayout.X_AXIS);

            JLabel nameL = new JLabel(l.who().name());
            nameL.setBorder(BorderFactory.createEmptyBorder(0,stdRowH,0,0));
            JPanel name = wPanel();
            name.setSize(new Dimension(logs1W, stdRowH));
            name.setPreferredSize(new Dimension(logs1W, stdRowH));
            name.setLayout(new BorderLayout());
            name.add(nameL, BorderLayout.WEST);

            JLabel amountL = new JLabel(String.format("%.2f", l.amount()));
            amountL.setBorder(BorderFactory.createEmptyBorder(0,16,0,16));
            JPanel amount = wPanel();
            amount.setSize(new Dimension(logs2W, stdRowH));
            amount.setPreferredSize(new Dimension(logs2W, stdRowH));
            amount.setLayout(new BorderLayout());
            amount.add(amountL, BorderLayout.WEST);

            String toWhomStr = l.toWhom().stream().map(p -> p.name()).collect(Collectors.joining(", "));
            JLabel toWhomL = new JLabel(toWhomStr);
            toWhomL.setBorder(BorderFactory.createEmptyBorder(0,16,0,0));
            JPanel toWhom = wPanel();
            toWhom.setSize(new Dimension(logs3W-stdRowH, stdRowH));
            toWhom.setPreferredSize(new Dimension(logs3W-stdRowH, stdRowH));
            toWhom.setLayout(new BorderLayout());
            toWhom.add(toWhomL, BorderLayout.WEST);

            element.add(name);
            element.add(amount);
            element.add(toWhom);
            element.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, lightGray));
            CrossButton delBtn = new CrossButton(stdRowH, 0.5, ""+(index++), CrossButton.DEL, removeLog);
            element.addMouseListener(new RmBtnMouseListener(element, 3));
            element.add(delBtn);
            element.setMaximumSize(new Dimension(logsWidth, stdRowH));
            toReturn.add(element);
        }


        return toReturn;
    }

    private void updatePeople(JComponent people){
        System.out.println("updating");
        people.removeAll();
        JComponent columnPeople = column(logger, "People", new Dimension(peopleWidth, widnowheight- addBtnSize-addBtnPadding), debtsList());
        System.out.println(people.getHeight()+"     column : "+people.getHeight());
        columnPeople.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
        people.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
        people.add(columnPeople, BorderLayout.CENTER);

        CrossButton addNameButton = new CrossButton(addBtnSize, 1, "add", CrossButton.ADD, s -> {
//            JOptionPane.showOptionDialog(null, 
//                    "Do you like this answer?", 
//                    "Feedback", 
//                    JOptionPane.OK_CANCEL_OPTION, 
//                    JOptionPane.INFORMATION_MESSAGE, 
//                    null, 
//                    new String[]{"Yes I do", "No I don't"}, // this is the array
//                    "default");
            String response = JOptionPane.showInputDialog(null, "How do you want to call your friend?");
            if(response != null){
                try {
                    logger.addPerson(response);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                JOptionPane.showMessageDialog(null, "No worries. Nothing has been done.");
                return;
            }
            SwingUtilities.invokeLater(() -> {updatePeople(people);});
        });

        addNameButton.addMouseListener(new AddBtnMouseListener(addNameButton));
        addNameButton.setSize(new Dimension(addBtnSize, addBtnSize));
        JPanel addBtnPane = wPanel();
        addBtnPane.setBorder(BorderFactory.createEmptyBorder(0, addBtnPadding, addBtnPadding, addBtnPadding));
        addBtnPane.setLayout(new BorderLayout());
        addBtnPane.add(addNameButton, BorderLayout.EAST);

        people.add(addBtnPane, BorderLayout.SOUTH);

        people.repaint(); people.revalidate(); people.doLayout();
    }

    private JComponent debtsList(){
        JPanel toReturn = boxPanel(BoxLayout.Y_AXIS);

        for(Map.Entry<Person, Map<Person, Double>> e : logger.getDebts().entrySet()){

            JPanel element = boxPanel(BoxLayout.Y_AXIS);

            JPanel name = boxPanel(BoxLayout.X_AXIS);
            name.addMouseListener(new RmBtnMouseListener(name, 1));

            name.setLayout(new BorderLayout());
            JLabel nameLab = new JLabel(e.getKey().name());
            nameLab.setFont(stdFont);
            nameLab.setBorder(BorderFactory.createEmptyBorder(0, stdRowH, 0, 0));
            name.add(nameLab, BorderLayout.WEST);
            name.setPreferredSize(new Dimension(peopleWidth, stdRowH));
            name.setMaximumSize(new Dimension(peopleWidth, stdRowH));
            name.add(new CrossButton(stdRowH, .5, e.getKey().name(), CrossButton.DEL, removeName), BorderLayout.EAST);
            element.add(name);

            Set<Map.Entry<Person, Double>> entrySet = new HashSet<>(e.getValue().entrySet());
            if(entrySet.isEmpty()){
                entrySet.add(null);
            }
            for(Map.Entry<Person, Double> d : entrySet){

                JPanel debt = boxPanel(BoxLayout.X_AXIS);
                debt.setPreferredSize(new Dimension(peopleWidth, stdRowH));
                debt.setMaximumSize(new Dimension(peopleWidth, stdRowH));

                JPanel value = wPanel();
                value.setLayout(new BorderLayout());
                JLabel valueLabel = new JLabel((d == null)? "" : String.format("%.2f",d.getValue()));
                valueLabel.setFont(stdFont);
                valueLabel.setBorder(BorderFactory.createEmptyBorder(0,0,0,stdRowH/2));
                value.add(valueLabel, BorderLayout.EAST);
                value.setPreferredSize(new Dimension(debtsLeftW, stdRowH));

                debt.add(value);

                JPanel namePane = wPanel();
                JLabel nameLabel = new JLabel((d == null)? "All Good" : d.getKey().name());
                nameLabel.setFont(stdFont);
                nameLabel.setBorder(BorderFactory.createEmptyBorder(0,stdRowH/2,0,0));

                namePane.setLayout(new BorderLayout());
                namePane.add(nameLabel, BorderLayout.WEST);
                namePane.setBorder(BorderFactory.createMatteBorder(0, peopleVertBar, 0, 0, (d == null)? green : red));
                namePane.setPreferredSize(new Dimension(debtsRightW, stdRowH));

                debt.add(namePane);
                element.add(debt);
            }
            element.setMinimumSize(new Dimension(peopleWidth, 2));

            element.setBorder(BorderFactory.createMatteBorder(0,0,1,0, lightGray));
            toReturn.add(element);
        }
        toReturn.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, lightGray));
        toReturn.setPreferredSize(new Dimension(peopleWidth, widnowheight));
        return toReturn;
    }

    public static JComponent column(Logger logger, String title, Dimension d, JComponent component){
        JPanel toReturn = wPanel();
        toReturn.setLayout(new BorderLayout());
        
        toReturn.setPreferredSize(d);
        JPanel titlePane = wPanel();
        titlePane.setPreferredSize(new Dimension((int)d.getWidth(), 70));
//        titlePane.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, red));
        JLabel titleLabel = new JLabel(title);
        titleLabel.setFont(TitleFont);
        titleLabel.setBorder(BorderFactory.createEmptyBorder(0,16,0,0));
        titlePane.setLayout(new BorderLayout());
        titlePane.add(titleLabel, BorderLayout.WEST);
        toReturn.setMinimumSize(d);

        toReturn.add(titlePane, BorderLayout.NORTH);
        toReturn.add(component, BorderLayout.CENTER);
        return toReturn;
    }

    public static JPanel boxPanel(int policy){
        JPanel toReturn = wPanel();
        toReturn.setLayout(new BoxLayout(toReturn, policy));
        return toReturn;
    }

    public static JPanel wPanel(){
        JPanel toReturn = new JPanel();
        toReturn.setBackground(Color.WHITE);
        return toReturn;
    }
}
