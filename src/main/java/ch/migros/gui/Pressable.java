package ch.migros.gui;

public interface Pressable {
    public void pressed();
}
