package ch.migros.gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;

public class RmBtnMouseListener extends MouseAdapter{
    private JComponent c;
    private int index;
    
    public RmBtnMouseListener(JComponent c, int index){
        this.c = c;
        this.index = index;
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
        CrossButton cB = (CrossButton)c.getComponent(index);
        cB.visible(true);
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
        CrossButton cB = (CrossButton)c.getComponent(index);
        cB.visible(false);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        Pressable p = (Pressable)c.getComponent(index);
        p.pressed();
    }
}








