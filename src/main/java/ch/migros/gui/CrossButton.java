package ch.migros.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.function.Consumer;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import ch.migros.utilitary.Logger;

@SuppressWarnings("serial")
public class CrossButton extends JComponent implements Pressable{
    private int w;
    private double scale;
    private String id;
    private Consumer<String> c;
    private final int style;
    private boolean visible = false;
    
    public static final int ADD = 0;
    public static final int DEL = 1;

    public CrossButton(int w, double s, String id, int style, Consumer<String> c){
        this.w = w;
        this.scale = s;
        this.style = style;
        this.id = id;
        this.c = c;
        visible = style == ADD;
    }

    public Dimension getPreferredSize(){
        return new Dimension(w, w);
    }
    
    public void visible(boolean set){
        visible = set;
        repaint();
    }

    public void paintComponent(Graphics g0){
        Graphics2D g = (Graphics2D)g0;
        if(visible){
            g.setColor((style == ADD)? GUIManager.green : GUIManager.red);
            int oWidth = (int)((w/2)*(1-scale));
            g.fillOval(oWidth, (int)((w/2)*(1-scale)), (int)(w*scale), (int)(w*scale));

            g.setColor(Color.WHITE);
            if (style != ADD) {
                double x1 = (w / 2) - (w / 2) * (scale) / 3;
                double x2 = (w) - x1;
                g.drawLine((int) x1, (int) x1, (int) x2, (int) x2);
                g.drawLine((int)x1, (int)x2, (int)x2, (int)x1);
            } else{
                double x1 = (w / 2) - (w / 2) * (scale) / 2;
                double x2 = (w) - x1;
                g.drawLine((int)(w/2), (int)x2, (int) (w/2), (int)x1);
                g.drawLine((int)x1, (int)(w/2), (int)x2, (int)(w/2));
            }
        }else{
            g.setBackground(Color.WHITE);
        }
    }
    
    public void pressed(){
        System.out.println("pressed");
        c.accept(id);
    }
}
